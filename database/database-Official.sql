CREATE TABLE administrateur (
    idadmin BIGSERIAL PRIMARY KEY NOT NULL,
    email varchar(255) NOT NULL,
    motdepasse varchar(255) NOT NULL
);

create table categorie (
    id bigserial primary key,
    type varchar(255) not null
);

create table etat (
    id bigserial primary key,
    type text not null,
    valeur bigint not null
);

create table genre (
    id bigserial primary key,
    libelle text not null
);
create table client (
    id bigserial primary key,
    nom text not null,
    telephone text not null,
    idgenre bigint references genre (id) not null,
    motdepasse text not null
);

create table commission (
    id bigserial primary key,
    idcategorie bigint references categorie (id),
    pourcentage double precision not null
);

create table compte (
    id bigserial primary key,
    idclient bigint references client (id),
    password text,
    solde double precision
);

create table enchere (
    id bigserial primary key,
    idclient bigint references client(id),
    idcategorie bigint references categorie(id),
    nomproduit text not null,
    dateheuredebut timestamp not null,
    description text,
    dateheurefin timestamp not null,
    prixdepart double precision not null
);

create view v_enchere as select *, case when now() between e.dateheuredebut and e.dateheurefin then 'en cours' when now() < dateheuredebut then 'pas encore commence' when now() > dateheurefin then 'termine'  end etat from enchere e;

create table photo (
    id bigserial primary key,
    idenchere bigint references enchere (id),
    base64image text not null
);

create table enchereduree (
    id bigserial primary key,
    duree double precision
);

create table demanderecharge (
    id bigserial primary key,
    idclient bigint references client (id),
    idetat bigint references etat (id),
    montant double precision,
    datedemanderecharge date default now()
);

CREATE TABLE historique (
    idhistorique BIGSERIAL PRIMARY KEY NOT NULL,
    idenchere BIGINT NOT NULL,
    idclient BIGINT NOT NULL,
    prix float8 NOT NULL
);

ALTER TABLE enchere ADD FOREIGN KEY (idclient) REFERENCES client (id);
ALTER TABLE enchere ADD FOREIGN KEY (idcategorie) REFERENCES categorie (idcategorie);
ALTER TABLE enchere ADD FOREIGN KEY (idetat) REFERENCES etat (idetat);

create or replace view statistique as
select enchere.nomproduit,
    enchere.dateheuredebut,
    enchere.prixdepart,
    client.nom,
    categorie.id
from enchere
    join client on client.id = enchere.idclient
    join categorie on categorie.id = enchere.idcategorie;